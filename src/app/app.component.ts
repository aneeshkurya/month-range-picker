import { Component, OnInit  } from '@angular/core';
declare var jquery:any;
declare var $ :any;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Month Range Picker!';

  public ngOnInit()
  { 
    var current_date = new Date();
    var current_month = current_date.getMonth();
    var current_year = current_date.getFullYear();
    $('input').rangePicker({ minDate:[current_month,current_year-2], maxDate:[current_month,current_year] })
        // subscribe to the "done" event after user had selected a date
        .on('datePicker.done', function(e, result){
            console.log(result);
        });
  }
}
